class RecurringTaskGenerator < ApplicationRecord
    belongs_to :task, dependent: :destroy
    validates :last_date, :end_date, :interval, presence: true
    validates :task, presence: true
    validates :end_date_must_be_later_than_last_date
    def end_date_must_be_later_than_last_date
        if last_date.present? && end_date.present? && (last_date > end_date)
            errors.add(:end_date, " must be later than last date.")
        end
    end
end
