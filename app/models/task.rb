class Task < ApplicationRecord
    has_and_belongs_to_many :tags, -> { distinct }
    has_many :recurring_task_generators
    has_one :super_task, class_name: "Task"
    has_many :sub_tasks, class_name: "Task", foreign_key: "parent_id"
    validates :name, :deadline, presence:true
    validates :priority, :inclusion => 1..5, allow_nil: true
end
