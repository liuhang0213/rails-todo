class TasksController < ApplicationController

    def index
        @tasks = Task.all
        @tags = Tag.all
    end

    def show
        @task = Task.find(params[:id])
    end

    def new
        respond_to :html, :js
        @task = Task.new
    end

    def edit
        respond_to :html, :js
        @task = Task.find(params[:id])
    end

    def create
        @task = Task.new(task_params)
        if @task.save
            add_tags @task,
                ActiveSupport::JSON.decode(params[:task][:tags])
            redirect_to @task
        else
            @tasks = Task.all
            @tags = Tag.all
            render 'index'
        end
    end

    def update
        @task = Task.find(params[:id])
        if @task.update(task_params)
            remove_all_tags @task
            add_tags @task,
                ActiveSupport::JSON.decode(params[:task][:tags])
            destroy_unused_tags
            redirect_to @task
        else
            render 'edit'
        end

    end

    def destroy
        @task = Task.find(params[:id])
        @task.destroy
        destroy_unused_tags
        redirect_to tasks_path
    end

    def destroy_unused_tags
        Tag.all.each do |tag|
            if tag.tasks.empty?
                tag.destroy
            end
        end
    end

    def add_tags(task, tags_hash)
        tags_hash.each do |tag_name|
            tag = Tag.find_by name: tag_name
            if (tag)
                task.tags << tag
            else
                task.tags.create('name' => tag_name)
            end
        end
    end

    def get_tags_array
        tags = Tag.all
        # -1 means no tags
        tags_array = [['-', -1]]
        tags.each do |tag|
            tags_array << [tag.name, tag.id]
        end
        return tags_array
    end

    def remove_all_tags(task)
        task.tags.clear
    end

    private
        def task_params
            params.require(:task).permit(:name, :detail, :deadline)
        end
end
