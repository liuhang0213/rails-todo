class TagsController < ApplicationController

    def index
        @tags = Tag.all
    end

    def show
        @tags = Tag.all
        @selected_tag = Tag.find(params[:id])
        @tasks = @selected_tag.tasks
        render 'tasks/index'
    end

    def new
        @tag = Tag.new
    end

    def edit
        respond_to do |format|
            format.js {}
            format.html
        end
        @tag = Tag.find(params[:id])
    end

    def create
        @tag = Tag.new(tag_params)
        if @tag.save
            redirect_to tasks_path
        else
            render 'new'
        end
    end

    def update
        @tag = Tag.find(params[:id])

        if @tag.update(tag_params)
            redirect_to @tag
        else
            render 'edit'
        end
    end

    def destroy
        @tag = Tag.find(params[:id])
        @tag.destroy

        redirect_to tags_path
    end

    private
        def tag_params
            params.require(:tag).permit(:name)
        end
end
