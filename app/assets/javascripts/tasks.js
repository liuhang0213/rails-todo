function add_fixed_tag() {
    // "Solidify" the entered tag
    if (/\S/.test($('#tag_input').val()) && $('#tag_input').val()) {
        // Check if the tag input is empty, or consists of only spaces
        $('#tags_display').append("<span class=\"tag tag_fixed\" onclick=\"remove_tag(this)\">"
            +$('#tag_input').val().trim().replace(',', '')
            +"</span>");
            $('#tag_input').val('');
    } else {}
}

function remove_tag(element) {
    $(element).remove();
}

function prepare_form() {
    $('.tag_fixed').bind('click', function() {
        remove_tag($(this));
    });
    $('#tag_input').bind('blur', add_fixed_tag);
    $('#new_task_form').submit(function(e) {
        var submit = true;
        if (!(/\S/.test($('#task_name').val()) && $('#task_name').val())) {
            // Check if name is empty
            $('#nameError').css("display", 'inline');
            submit = false;
        } else {
            $('#nameError').css("display", 'none');
        }

        if (!$('#deadline').val()) {
            // Check is deadline is empty
            $('#deadlineError').css("display", 'inline');
            submit = false;
        } else {
            $('#deadlineError').css("display", 'none');
        }

        if (submit) {
            var tags = [];
            $('.tag_fixed').each(function(id) {
                tags.push($(this).html());
            });
            $('#tags').val(JSON.stringify(tags));
            if (tags.length == 0) {
                $('#tags').val('{}');
            }
        } else {
            e.preventDefault();
        }
        return submit;
    });

    $('#tag_input').keyup(function(e) {
        if (e.keyCode === 188) {
            add_fixed_tag()
        } else {}
    });
}

function confirm_delete(id) {
    var confirm = window.confirm("Do you want to delete the post?");
    if (confirm) {

    } else {}
}
