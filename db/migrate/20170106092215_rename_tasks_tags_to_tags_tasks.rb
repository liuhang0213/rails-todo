class RenameTasksTagsToTagsTasks < ActiveRecord::Migration[5.0]
  def change
      rename_table :tasks_tags, :tags_tasks
  end
end
