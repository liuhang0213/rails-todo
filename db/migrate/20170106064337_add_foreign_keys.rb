class AddForeignKeys < ActiveRecord::Migration[5.0]
  def change
      add_index :tasks, :super_task_id
      add_index :recurring_task_generators, :task_id
      create_table :tasks_tags, id: false do |t|
          t.integer :task_id
          t.integer :tag_id
      end
      add_index :tasks_tags, :task_id
      add_index :tasks_tags, :tag_id
  end
end
