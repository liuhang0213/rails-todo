class AddForeignKeyToTasks < ActiveRecord::Migration[5.0]
  def change
      rename_column :tasks, :parent, :parent_id
  end
end
