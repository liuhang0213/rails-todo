class CreateRecurringTaskGenerators < ActiveRecord::Migration[5.0]
  def change
    create_table :recurring_task_generators do |t|
      t.datetime :last_date
      t.datetime :end_date
      t.integer :interval

      t.timestamps
    end
  end
end
