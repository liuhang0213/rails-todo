Rails.application.routes.draw do
    resources :tasks
    resources :tags
    root 'tasks#index'
end
